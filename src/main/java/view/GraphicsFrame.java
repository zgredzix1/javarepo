package view;
import javax.swing.*;
import org.math.plot.*;
import java.awt.*;

/**
 * class GraphicsFrame extends JFrame Represents program second frame
 * Frame where we can see the graphic representation of a complex number
 * @see JFrame
 * @author Igor Rudnicki
 */
public class GraphicsFrame extends JFrame {

    /**int parameter representing the change on X axe*/
    private int x;
    /**int parameter representing the change on Y axe*/
    private int y;
    /**
     * Constructor of class GraphicsFrame
     * creating a new frame, setting parameters x and y, creating new JPanel with new parameters x,y
     * creating a new plot using implemented function createPlot
     * @param x int parameter representing the change on X axe
     * @param y int parameter representing the change on Y axe
     * @see JFrame
     */
    public GraphicsFrame(int x, int y)  {
        super("Graphic representation");

        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        double width = screenSize.getWidth();
        double height = screenSize.getHeight();

        this.x = x;
        this.y = y;

        createPlot(x,y);

        super.setVisible(true);
        super.setLocation((int)width/2+225,(int)height/2-250);
        super.setSize(500, 500);


    }

    /**
     * method createPlot creates a new plot using jmathplot library
     * presenting a complex number
     * @param x int parameter representing the change on X axe
     * @param y int parameter representing the change on Y axe
     */
    private void createPlot(int x,int y)
    {
        System.out.println(x);
        System.out.println(y);
        double[] X = { 0, 0};
        double[] Y = { x, y};

        // create your PlotPanel (you can use it as a JPanel)
        Plot2DPanel plot = new Plot2DPanel();
        // add a line plot to the PlotPanel
        plot.addLinePlot("Complex Graphic Representation",Color.ORANGE, X, Y);
        plot.setAxisLabels("Real","Imaginary");
        plot.getAxis(0).setLabelPosition(0.95,0.45);
        plot.getAxis(1).setLabelPosition(0.38,0.97);
        // define the legend position
        plot.addLegend("SOUTH");
        plot.setBackground(Color.ORANGE);
        plot.getAxis(0).setColor(Color.ORANGE);
        plot.getAxis(1).setColor(Color.ORANGE);
        plot.getAxis(0).setLabelFont(new Font("SansSerif", Font.BOLD, 15));
        plot.getAxis(1).setLabelFont(new Font("SansSerif", Font.BOLD, 15));
        plot.setFixedBounds(0,-50,50);
        plot.setFixedBounds(1,-50,50);



        super.setContentPane(plot);
    }


}
