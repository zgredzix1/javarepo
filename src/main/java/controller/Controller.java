package controller;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.math3.exception.MathParseException;
import view.CalculatorFrame;
import view.Dialogs;
import view.GraphicsFrame;
import model.ComplexM;
import model.Utils;
import javax.swing.*;

/**
 * class Controller implements ActionListener
 * Used to handle all of the events created while clicking some buttons or typing some texts
 * Honestly containing all of the program's logic
 * @author Igor Rudnicki
 * @see ActionListener
 * @see ArrayList
 */
public class Controller  {
    
    /**
     * first complex used in the operation
     */
    private ComplexM z1 = new ComplexM();
    /**
     * second complex used in the operation
     */
    private ComplexM z2 = new ComplexM();
    /**
     * result of the whole operation
     */
    private ComplexM zresult = new ComplexM();
    /**
     * temporary complex number used in some repeated operations
     */
    private ComplexM ztemp = new ComplexM();
    /**
     * static variable signifying each operation
     */
    private static int operator = 0;
    /**
     * static variable signifying a counter of equalAction method calls
     */
    private static int counter = 0;
    /*variables x and y represent changes on axes values*/
    private int x, y;
    /**
     * ArrayList containing all created graphic frames
     */
    private List<GraphicsFrame> glist = new ArrayList<GraphicsFrame>();
    /**
     * a maximum value of a real or imaginary part of a complex
     */
    private final double MAX_VALUE = 1e10;
    /**
     * left border of the chart, minimum real part of a complex
     */
    private final int XLEFT = -50;
    /**
     * right border of the chart, maximum real part of a complex
     */
    private final int XRIGHT = 50;
    /**
     * left border of the chart, minimum real part of a complex
     */
    private final int YDOWN = -50;
    /**
     * down border of the chart, minimum imaginary part of a complex
     */
    private final int YUP = 50;

    /**
     * variable signifying a power in exponenting operation
     */
    private double k;
    /**
     * temporary variable signifying a power in exponenting operation
     */
    private double tempk;

    /**
     * object of class CalculatorFrame
     */
    private CalculatorFrame cp;
    /**
     * object of class Utils
     */

    /**
     * Constructor of class Controller
     * adding ActionListeners to all JButtons,JTextfields
     *
     * @param cp Forwarded object of class CalculatorFrame
     */

    public Controller(CalculatorFrame cp) {
        this.cp = cp;
        cp.addSumListener(new SumListener());
        cp.addGraphListener(new GraphListener());
        cp.addExitListener(new ExitListener());
        cp.addDifferListener(new DifferListener());
        cp.addDivListener(new DivListener());
        cp.addMultListener(new MultListener());
        cp.addExpListener(new ExpListener());
        cp.addEqualListener(new EqualListener());
        cp.addBackListener(new BackListener());
        cp.addCoupListener(new CoupListener());
        cp.addInputListener(new InputListener());
        cp.addClearListener(new ClearListener());
    }
    private class SumListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            if(cp.checkEmptyInput()==0)
                addingAction();
        }
    }
    private class GraphListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            if(cp.checkEmptyInput()==0)
                graphAction();
        }
    }
    private class ExitListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            System.exit(0);
        }
    }
    private class DifferListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            if (cp.checkEmptyInput()==0)
                differAction();
        }
    }
    private class MultListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            if(cp.checkEmptyInput()==0)
                multAction();
        }
    }
    private class DivListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            if(cp.checkEmptyInput()==0)
                divideAction();
        }
    }
    private class ExpListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            if(cp.checkEmptyInput()==0)
                expAction();
        }
    }
    private class CoupListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            if(cp.checkEmptyInput()==0)
                coupAction();
        }
    }
    private class EqualListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            if(cp.checkEmptyInput()==0)
                equalAction();
        }
    }
    private class BackListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            if(cp.checkEmptyInput()==0)
                backAction();
        }
    }
    private class ClearListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
                cp.setTField("");
                cp.setLNum("...");
        }
    }
    private class InputListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            if(cp.checkEmptyInput()==0)
                inputAction();
        }
    }


    /**
     * updating  x and y parameters read from JTextField- tDataInput
     * printing them in console
     */
    private void changeLine() {
        ComplexM z = new ComplexM();
        String s;
        s = cp.getTField();
        try {
            z = Utils.stringConvert(s);
        }
        catch(MathParseException e)
        {
            return ;
        }
        x = (int) z.getReal();
        y = (int) z.getImag();
        System.out.println(x);
        System.out.println(y);


    }

    /**
     * method used while clicking graph button, creates a new frame, and updates it
     */
    private void graphAction() {
        changeLine();
        ComplexM z = new ComplexM();
        String s;
        s = cp.getTField();
        try {
            z = Utils.stringConvert(s);
        } catch (MathParseException e) {
            return;
        }
        if (checkBoundaries(z) == -1)
            return;
        else {
            if (glist.size() > 0)
                glist.get(glist.size() - 1).setVisible(false);

            GraphicsFrame gFrame = new GraphicsFrame(x, y);
            glist.add(gFrame);
            gFrame.repaint();
        }
    }


    /**
     * method used while some text is being typed in the textfield, converting a text into a complex using function stringConvert
     */
    private void inputAction() {
        String s;
        s = cp.getTField();
        z1 = Utils.stringConvert(s);
    }

    /**
     * method enabling undo character by character in the textfield
     */
    private void backAction() {
        int length = cp.getTField().length();
        int number = cp.getTField().length() - 1;
        String store;
        if (length > 0) {
            StringBuilder back = new StringBuilder(cp.getTField());
            back.deleteCharAt(number);
            store = back.toString();
            cp.setTField(store);
        }
    }

    /**
     * method used while clicking on the add button, operation of adding two complex
     * checking if an exception occured and checking other statements
     */
    private void addingAction() {
        counter = 0;
        String s = cp.getTField();
        try {
            z1 = Utils.stringConvert(s);
            if (checkValue(z1) == -1) {
                z1.setReal(0);
                z1.setImag(0);
            } else {
                cp.setTField("");
                cp.setLNum(s + " +");
            }
        } catch (MathParseException e) {
            Dialogs d = new Dialogs("NOT A COMPLEX NUMBER");
            z1.setReal(0);
            z1.setImag(0);
            cp.setTField("");
            cp.setLNum("");
        }
        catch (NumberFormatException e) {
            Dialogs d = new Dialogs("NOT A COMPLEX NUMBER");
            z1.setReal(0);
            z1.setImag(0);
            cp.setTField("");
            cp.setLNum("");
        }
        operator = 1;

    }

    /**
     * method used while clicking on the differ button, operation of differing two complex
     * checking if an exception occured and checking other statements
     */
    private void differAction() {
        counter = 0;
        String s = cp.getTField();
        try {
            z1 = Utils.stringConvert(s);
            if (checkValue(z1) == -1) {

                z1.setReal(0);
                z1.setImag(0);

            } else {
                cp.setTField("");
                cp.setLNum(s + " -");
            }
        } catch (MathParseException e) {
            Dialogs d = new Dialogs("NOT A COMPLEX NUMBER");
            z1.setReal(0);
            z1.setImag(0);
            cp.setTField("");

        }
        catch (NumberFormatException e) {
            Dialogs d = new Dialogs("NOT A COMPLEX NUMBER");
            z1.setReal(0);
            z1.setImag(0);
            cp.setTField("");

        }
        operator = 2;
    }

    /**
     * method used while clicking on the mult button, operation of multiplying two complex
     * checking if an exception occured and checking other statements
     */
    private void multAction() {
        counter = 0;
        String s = cp.getTField();
        try {
            z1 = Utils.stringConvert(s);
            if (checkValue(z1) == -1) {

                z1.setReal(0);
                z1.setImag(0);

            } else {
                cp.setTField("");
                cp.setLNum(s + " *");
            }
        } catch (MathParseException e) {
            Dialogs d = new Dialogs("NOT A COMPLEX NUMBER");
            z1.setReal(0);
            z1.setImag(0);
            cp.setTField("");
        }
        catch (NumberFormatException e) {
            Dialogs d = new Dialogs("NOT A COMPLEX NUMBER");
            z1.setReal(0);
            z1.setImag(0);
            cp.setTField("");
        }
        operator = 3;

    }

    /**
     * method used while clicking on the div button, operation of dividing two complex
     * checking if an exception occured and checking other statements
     */
    private void divideAction() {
        counter = 0;
        String s = cp.getTField();
        try {
            z1 = Utils.stringConvert(s);
            if (checkValue(z1) == -1) {

                z1.setReal(0);
                z1.setImag(0);

            } else {
                cp.setTField("");
                cp.setLNum(s + " /");
            }
        } catch (MathParseException e) {
            Dialogs d = new Dialogs("NOT A COMPLEX NUMBER");
            z1.setReal(0);
            z1.setImag(0);
            cp.setTField("");
        }
        catch (NumberFormatException e) {
            Dialogs d = new Dialogs("NOT A COMPLEX NUMBER");
            z1.setReal(0);
            z1.setImag(0);
            cp.setTField("");
        }
        operator = 4;
    }

    /**
     * method used while clicking on the exp button, operation of exponenting a complex to given power
     * checking if an exception occured and checking other statements
     */
    private void expAction() {
        counter = 0;
        String s = cp.getTField();
        try {
            z1 = Utils.stringConvert(s);
            if (checkValue(z1) == -1) {

                z1.setReal(0);
                z1.setImag(0);

            } else {
                cp.setTField("");
                cp.setLNum(s + " ()^");
            }
        } catch (MathParseException e) {
            Dialogs d = new Dialogs("NOT A COMPLEX NUMBER");
            z1.setReal(0);
            z1.setImag(0);
            cp.setTField("");
        }
        catch (NumberFormatException e) {
            Dialogs d = new Dialogs("NOT A COMPLEX NUMBER");
            z1.setReal(0);
            z1.setImag(0);
            cp.setTField("");
        }
        operator = 5;
    }

    /**
     * method used while clicking on the coup button, operation of coupling a complex
     * checking if an exception occured and checking other statements
     */
    private void coupAction() {
        String s = cp.getTField();
        try {
            z1 = Utils.stringConvert(s);
            zresult = zresult.coupling(z1);
            z1 = zresult;
            if (checkValue(z1) == -1) {
                z1.setReal(0);
                z1.setImag(0);
            } else {
                cp.setTField("" + zresult);
                cp.setLNum("");
            }
        } catch (MathParseException e) {
            Dialogs d = new Dialogs("NOT A COMPLEX NUMBER");
            z1.setReal(0);
            z1.setImag(0);
            cp.setTField("");
        }
        catch (NumberFormatException e) {
            Dialogs d = new Dialogs("NOT A COMPLEX NUMBER");
            z1.setReal(0);
            z1.setImag(0);
            cp.setTField("");
        }
        operator = 6;
    }

    /**
     * method used while clicking on the equal button, checking which operation was called(in switch)
     * displaying the results in the texfield
     * catching exceptions if occured, and also other statements
     * checking if an exception occured and checking other statements
     */
    private void equalAction() {
        String s = cp.getTField();
        ++counter;
        try {
            z2 = Utils.stringConvert(s);
            if (counter == 1)
                ztemp = z2;
        } catch (MathParseException e) {
            Dialogs d = new Dialogs("NOT A COMPLEX NUMBER");
            cp.setTField("");
            --counter;
            return;
        }
            catch(NumberFormatException e)
            {
                Dialogs d = new Dialogs("NOT A COMPLEX NUMBER");
                cp.setTField("");
                --counter;
                return;
            }

        if (z2.getReal() == 0 && z2.getImag() == 0 && operator == 4) {
            Dialogs d = new Dialogs("CANNOT DIVIDE BY 0");
            cp.setTField("");
            --counter;
            return;
        }
        if (z2.getImag() > MAX_VALUE || z2.getImag() < -MAX_VALUE || z2.getReal() < -MAX_VALUE || z2.getReal() > MAX_VALUE) {
            Dialogs d = new Dialogs("NUMBER IS TOO BIG");
            cp.setTField("");
            --counter;
            return;
        }

        switch (operator) {
            case 1:

                if (counter > 1) {
                    zresult = zresult.add(z2, ztemp);
                } else
                    zresult = zresult.add(z1, z2);

                break;

            case 2:

                if (counter > 1) {
                    zresult = zresult.diff(z2, ztemp);
                } else
                    zresult = zresult.diff(z1, z2);

                break;

            case 3:
                if (counter > 1) {
                    zresult = zresult.multiply(z2, ztemp);
                } else
                    zresult = zresult.multiply(z1, z2);

                break;

            case 4:
                if (counter > 1) {
                    zresult = zresult.divide(z2, ztemp);
                } else
                    zresult = zresult.divide(z1, z2);

                break;
            case 5:
                if (counter == 1) {
                    k = Double.parseDouble(cp.getTField());
                    tempk = k;
                    zresult = z1.exponent(z1, k);
                    ztemp = zresult;
                } else if (counter > 1) {
                    zresult = zresult.exponent(ztemp, tempk);
                    ztemp = zresult;
                }
                break;
            case 6:
                zresult = z1.coupling(z1);
                break;
            default:
                break;
        }
        //if(checkValue(zresult)==-1)


        cp.setTField("" + zresult);
        cp.setLNum("...");
    }

    /**
     * method checking whether the value is too big,exceeding the maximum final values
     */
    private int checkValue(ComplexM z1) {

        if (z1.getImag() > MAX_VALUE || z1.getImag() < -MAX_VALUE || z1.getReal() > MAX_VALUE || z1.getReal() < -MAX_VALUE) {
            cp.setTField("");
            cp.setLNum("");
            Dialogs d = new Dialogs("NUMBER IS TOO BIG");
            return -1;

        } else
            return 0;
    }

    /**
     * method checking if the complex representation fits is the graphics frame
     */
    private int checkBoundaries(ComplexM z) {
        if (z.getReal() < XLEFT || z.getReal() > XRIGHT || z.getImag() < YDOWN || z.getImag() > YUP) {
            Dialogs d = new Dialogs("TOO BIG TO DISPLAY(-50;50)");
            return -1;
        } else
            return 0;
    }

}