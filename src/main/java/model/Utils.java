package model;
import org.apache.commons.math3.complex.Complex;
import org.apache.commons.math3.complex.ComplexFormat;
import org.apache.commons.math3.exception.MathParseException;

/**
 * class Utils Contains special functions assisting in program operations
 * method stringConvert(converts String to Complex)
 * method roundDouble(rounds a number to three decimal places)
 */
public class Utils {

    /**
     * method stringConvert Converts given String variable to object of class Complex
     * using methods of class String, f.e charAt(),contains()
     * if NumberFormatException is cought then it is thrown and further handled
     *
     * @param s String variable taken from the textfield
     * @return new model.Complex()
     */
    public static ComplexM stringConvert(String s) {
        boolean firstPositive = true;
        boolean secondPositive = true;
        double realPart = 0;
        double imgPart = 0;
        if(!s.contains(" "))
        {
        try {
            if (s.charAt(0) == '-' && !s.contains("i")) {
                String s2 = s.substring(1, s.length());
                realPart = Double.parseDouble(s2);
                return new ComplexM(-1 * realPart, 0);
            }
            if (s.charAt(0) == '-' && s.contains("i") && !s.substring(1, s.length() - 1).contains("-") && !s.substring(1, s.length() - 1).contains("+")) {

                imgPart = Double.parseDouble(s.substring(1, s.length() - 1));
                return new ComplexM(0, -1 * imgPart);
            }
            if (s.charAt(0) == '-')     // See if first expr is negative
                firstPositive = false;
            if (s.substring(1).contains("-"))
                secondPositive = false;
            String[] split = s.split("[+-]");
            if (split[0].equals("")) {  // Handle expr beginning with `-`
                split[0] = split[1];
                split[1] = split[2];
            }
            if (split[0].contains("i")) // Assumes input is not empty
                imgPart = Double.parseDouble((firstPositive ? "+" : "-") + split[0].substring(0, split[0].length() - 1));
            else
                realPart = Double.parseDouble((firstPositive ? "+" : "-") + split[0]);
            if (split.length > 1) {     // Parse second part of expr if it exists
                if (split[1].contains("i"))
                    imgPart = Double.parseDouble((secondPositive ? "+" : "-") + split[1].substring(0, split[1].length() - 1));
                else
                    realPart = Double.parseDouble((secondPositive ? "+" : "-") + split[1]);
            }
        } catch (NumberFormatException e) {
            throw new NumberFormatException("Not a complex number");

        }
        return new ComplexM(realPart, imgPart);
    }
        else{
            double real=0;
            double imag=0;
        if (s.contains(".")) {
            s = s.replace(".", ",");
            System.out.println(s);
        }
        try {
            Complex c = new ComplexFormat().parse(s);
            real = c.getReal();
            imag = c.getImaginary();

        } catch (MathParseException e) {
            throw new NumberFormatException("Not a complex number");
        }
        return new ComplexM(real, imag);
    }

    }

    /**
     * method doubleRound Rounds given number to three decimal numbers
     * @param x Double variable, number to round
     * @return new double number, rounded to 3 decimal numbers
     */
    public static double doubleRound(double x)
    {
        x*=1000;
        x=Math.round(x);
        x/=1000;
        return x;

    }
    public static boolean isDoubleInt(double d)
    {
        //select a "tolerance range" for being an integer
        double TOLERANCE = 1E-5;
        //do not use (int)d, due to weird floating point conversions!
        return Math.abs(Math.floor(d) - d) < TOLERANCE;
    }
}
