import java.awt.EventQueue;
/**
 * class Main
 * using method invokeLater to handle events
 * creating a new object of class CalculatorInit- initializing the calculator
 * @author Igor Rudnicki
 */
public class Main {
    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                new CalculatorInit();
            }
        });
    }
}