package view;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

/**
 * class CalculatorFrame extending JFrame
 * Represents an opening frame containing buttons and a textfield
 * complex calculator graphic interface ( GUI ) with the user
 * @see JFrame
 * @author Igor Rudnicki
 */
public class CalculatorFrame extends JFrame  {
    /**created JButton objects*/
    private JButton bSum,bExit,bDiffer,bMult,bExp,bDiv,bEquals,bCoup,bBack,bGraph,bClear;
    /**created JTextField object*/
    private JTextField tDataInput;
    /**created JLabel object*/
    private JLabel lNum;
    /** object of class GridBagConstraints used in the GridBagLayout*/
    private GridBagConstraints gbc =new GridBagConstraints();
    /**size of users screen*/
    private Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
    /**width  of the users screen*/
    private double width = screenSize.getWidth();
    /**height  of the users screen*/
    private double height = screenSize.getHeight();

    /**
     * constructor of the class CalculatorFrame
     * Creating new Frame and setting its parameters
     * Creating a GridBagLayout and setting its parameters
     * Creating objects of the JButton,JTextField and JLabel classes
     * Setting their parameters
     */
    public CalculatorFrame() {

        super("Complex Calculator");
        setLayout(new GridBagLayout());
        gbc.insets=new Insets(5,5,5,5);
        setSize(450, 500);
        // setLocation
        getContentPane().setBackground(Color.BLACK);
        setResizable(true);
        setLocation((int)width/2-225,(int)height/2-250);
        setVisible(true);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        bGraph = new JButton("GRAPH");
        gbc.gridx=2;
        gbc.gridy=6;
        gbc.fill= GridBagConstraints.HORIZONTAL;
        add(bGraph,gbc);
        bGraph.setForeground(Color.WHITE);
        bGraph.setBackground(Color.RED);
        bGraph.setFont(new Font("SansSerif", Font.BOLD, 25));

        bSum = new JButton("+");
        gbc.gridx=0;
        gbc.gridy=3;
        gbc.fill= GridBagConstraints.HORIZONTAL;
        add(bSum,gbc);
        bSum.setForeground(Color.BLACK);
        bSum.setBackground(Color.WHITE);
        bSum.setFont(new Font("SansSerif", Font.BOLD, 30));

        bDiffer = new JButton("-");
        gbc.gridx=1;
        gbc.gridy=3;
        gbc.weightx=1.5;
        add(bDiffer,gbc);
        bDiffer.setForeground(Color.BLACK);
        bDiffer.setBackground(Color.WHITE);
        bDiffer.setFont(new Font("SansSerif", Font.BOLD, 30));

        bDiv = new JButton("/");
        gbc.gridx=1;
        gbc.gridy=4;
        add(bDiv,gbc);
        bDiv.setForeground(Color.BLACK);
        bDiv.setBackground(Color.WHITE);
        bDiv.setFont(new Font("SansSerif", Font.BOLD, 30));

        bMult = new JButton("*");
        gbc.gridx=0;
        gbc.gridy=4;
        gbc.fill= GridBagConstraints.HORIZONTAL;
        add(bMult,gbc);
        bMult.setForeground(Color.BLACK);
        bMult.setBackground(Color.WHITE);
        bMult.setFont(new Font("SansSerif", Font.BOLD, 30));

        bExp = new JButton("()^");
        gbc.gridx=0;
        gbc.gridy=5;
        gbc.fill= GridBagConstraints.HORIZONTAL;
        add(bExp,gbc);
        bExp.setForeground(Color.BLACK);
        bExp.setBackground(Color.WHITE);
        bExp.setFont(new Font("SansSerif", Font.BOLD, 30));

        bClear = new JButton("C");
        gbc.gridx=2;
        gbc.gridy=3;
        gbc.gridwidth=1;
        gbc.fill= GridBagConstraints.HORIZONTAL;
        add(bClear,gbc);
        bClear.setForeground(Color.WHITE);
        bClear.setBackground(Color.ORANGE);
        bClear.setFont(new Font("SansSerif", Font.BOLD, 30));

        bBack = new JButton("<--");
        gbc.gridx=2;
        gbc.gridy=4;
        gbc.gridwidth=1;
        add(bBack,gbc);
        bBack.setForeground(Color.WHITE);
        bBack.setBackground(Color.ORANGE);
        bBack.setFont(new Font("SansSerif", Font.BOLD, 30));

        bCoup = new JButton("~");
        gbc.gridx=1;
        gbc.gridy=5;
        add(bCoup,gbc);
        bCoup.setForeground(Color.BLACK);
        bCoup.setBackground(Color.WHITE);
        bCoup.setFont(new Font("SansSerif", Font.BOLD, 30));

        bEquals = new JButton("=");
        gbc.gridx=2;
        gbc.gridy=5;
        add(bEquals,gbc);
        bEquals.setForeground(Color.WHITE);
        bEquals.setBackground(Color.ORANGE);
        bEquals.setFont(new Font("SansSerif", Font.BOLD, 30));

        bExit = new JButton("OFF");
        gbc.gridx=2;
        gbc.gridy=7;
        add(bExit,gbc);
        bExit.setForeground(Color.WHITE);
        bExit.setBackground(Color.ORANGE);
        bExit.setFont(new Font("SansSerif", Font.BOLD, 30));

        tDataInput = new JTextField("0");
        tDataInput.setFont(new Font("SansSerif", Font.BOLD, 40));
        tDataInput.setHorizontalAlignment(SwingConstants.RIGHT);
        tDataInput.setForeground(Color.WHITE);
        tDataInput.setBackground(Color.BLACK);
        tDataInput.setOpaque(true);
        gbc.fill= GridBagConstraints.HORIZONTAL;
        gbc.gridwidth=5;
        gbc.gridx=0;
        gbc.gridy=2;
        add(tDataInput,gbc);
        tDataInput.setToolTipText("Type complex number f.e '1+2i'");

        lNum = new JLabel("...");
        lNum.setFont(new Font("SansSerif", Font.BOLD, 25));
        lNum.setBackground(Color.WHITE);
        lNum.setForeground(Color.WHITE);
        gbc.fill= GridBagConstraints.HORIZONTAL;
        gbc.gridx=0;
        gbc.gridy=1;
        lNum.setHorizontalAlignment(SwingConstants.RIGHT);
        add(lNum,gbc);


    }

    /**method addSumListener adds new ActionListener to sum button
     * @param mal ActionListener
     */
    public void addSumListener(ActionListener mal) {
        bSum.addActionListener(mal);
    }
    /**method addGraphistener adds new ActionListener to graph button
     * @param mal ActionListener
     */
    public void addGraphListener(ActionListener mal) { bGraph.addActionListener(mal); }
    /**method addExitListener adds new ActionListener to exit button
     * @param mal ActionListener
     */
    public void addExitListener(ActionListener mal) {
        bExit.addActionListener(mal);
    }
    /**method addDifferListener adds new ActionListener to substract button
     * @param mal ActionListener
     */
    public void addDifferListener(ActionListener mal) {
        bDiffer.addActionListener(mal);
    }
    /**method addMultListener adds new ActionListener to multpiply button
     * @param mal ActionListener
     */
    public void addMultListener(ActionListener mal) {
        bMult.addActionListener(mal);
    }
    /**method addExpListener adds new ActionListener to exponentbutton
     * @param mal ActionListener
     */
    public void addExpListener(ActionListener mal) {
        bExp.addActionListener(mal);
    }
    /**method addDivListener adds new ActionListener to divide button
     * @param mal ActionListener
     */
    public void addDivListener(ActionListener mal) {
        bDiv.addActionListener(mal);
    }
    /**method addEqualListener adds new ActionListener to equal button
     * @param mal ActionListener
     */
    public void addEqualListener(ActionListener mal) {
        bEquals.addActionListener(mal);
    }
    /**method addBackListener adds new ActionListener to back button
     * @param mal ActionListener
     */
    public void addBackListener(ActionListener mal) { bBack.addActionListener(mal); }
    /**method addCoupListener adds new ActionListener to coupling button
     * @param mal ActionListener
     */
    public void addCoupListener(ActionListener mal)
    {
        bCoup.addActionListener(mal);
    }
    /**method addInputListener adds new ActionListener to input textfield
     * @param mal ActionListener
     */
    public void addInputListener(ActionListener mal)
    {
        tDataInput.addActionListener(mal);
    }
    /**method addClearListener adds new ActionListener to clear button
     * @param mal ActionListener
     */
    public void addClearListener(ActionListener mal) { bClear.addActionListener(mal); }

    /**method checkEmptyInput checks if dDataInput is empty
     * @return -1 if tDataInput empty 0 if not
     */
    public int checkEmptyInput()
    {
        if(tDataInput.getText().isEmpty())
            return -1;
        else
            return 0;
    }

    /**method setTField adds a new string to tDataInput
     * @param s String representing complex number
     */
    public void setTField(String s){
        tDataInput.setText(s);
    }
    /**method setLNum adds a new string to lNum
     * @param s String representing complex number
     */
    public void setLNum(String s){
        lNum.setText(s);
    }

    /**method getField returns a text
     * @return string represents a text from tDataInput
     */
    public String getTField(){ return tDataInput.getText(); }
}
