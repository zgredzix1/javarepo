/CONTENT/
Complex Calculator
Mathematical operations like adding,substracting,dividing,multiplying,coupling,exponenting and graphic visualisation
GUI with SWING library
Tests with JUnit library
Charts using JMathPlot library

/HOW TO BUILD AND RUN USING MAVEN/
*download the repository*
*open terminal in downloaded folder
*type    mvn clean  -> mvn compile -> mvn package
*then in subfolder target should appear a file called JavaProject-1.0-SNAPSHOT-jar-with-dependencies*
*if we want to run it then type       java -jar target/JavaProject-1.0-SNAPSHOT-jar-with-dependencies.jar *
*if we want to run junit tests then type mvn test*
