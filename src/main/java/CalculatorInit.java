import controller.Controller;
import view.CalculatorFrame;
import javax.swing.*;

/**
 * class CalculatorInit
 * loading a new LookAndFeel
 * creating a new object of class Calculator Frame
 * creating a new object of class Controller
 * initializing our complex calculator app
 * @author Igor Rudnicki
 */

public class CalculatorInit {


    public CalculatorInit() {
        try {
            for (UIManager.LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    UIManager.setLookAndFeel(info.getClassName());
                    break;
                }

            }
        } catch (Exception e) {
            System.out.println("Look and feel has not been loaded");
        }

        CalculatorFrame view = new CalculatorFrame();
        Controller controller = new Controller(view);


    }

}
