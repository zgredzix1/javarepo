import model.Utils;
import org.junit.After;
import org.junit.Test;
import org.junit.Assert;
import model.ComplexM;

/**
 * class CalculatorTest
 * implementing tests of addidtion,substraction,multiplying and dividing operations on complex numbers and tests of exponenting,coupling a complex number
 * implementing test checking dividing by zero,test checking the value (whether is too big) and test checking if the typed text is a complex number
 * implementing test checking if the charm fits in the frame
 * @author Igor Rudnicki
 */
public class OperationTest {

    /** complex 1 used in tested operations*/
    private ComplexM z1 = new ComplexM(2,3);
    /** complex 12 used in tested operations*/
    private ComplexM z2 = new ComplexM(1,1);
    /**left border of the chart, minimum real part of a complex*/
    private final int XLEFT=-50;
    /**right border of the chart, maximum real part of a complex*/
    private final int XRIGHT=50;
    /**left border of the chart, minimum real part of a complex*/
    private final int YDOWN=-50;
    /**down border of the chart, minimum imaginary part of a complex*/
    private final int YUP=50;
    /**static int ,incrementing with each test*/
    private static int testNumb=0;
    /**maximum value of a real or an imaginary part of a complex */
    private final double MAX_VALUE=1e10;
    /*object of class Utils used to use some methods */
    private Utils u;

    /**method called after each test*/
    @After
    public void afterTest()
    {
        System.out.println("End of test"+testNumb);
    }
    /**complex adding test*/
    @Test
    public void testAdd() {

        ComplexM zExpected = new ComplexM(3,4);
        ComplexM zResult =new ComplexM();
        zResult=zResult.add(z1,z2);
        testNumb=1;

        Assert.assertTrue(zExpected.equals(zResult));
    }
    /**complex differing test*/
    @Test
    public void testDiff() {

        ComplexM zExpected = new ComplexM(1,2);
        ComplexM zResult =new ComplexM();
        zResult=zResult.diff(z1,z2);
        testNumb=2;

        Assert.assertTrue(zExpected.equals(zResult));
    }
    /**complex multiplying test*/
    @Test
    public void testMultiply() {

        ComplexM zExpected = new ComplexM(-1,5);
        ComplexM zResult =new ComplexM();
        zResult=zResult.multiply(z1,z2);
        testNumb=3;

        Assert.assertTrue(zExpected.equals(zResult));
    }
    /**complex dividing test*/
    @Test
    public void testDivide() {

        ComplexM zExpected = new ComplexM(2.5,0.5);
        ComplexM zResult =new ComplexM();
        zResult=zResult.divide(z1,z2);
        testNumb=4;

        Assert.assertTrue(zExpected.equals(zResult));
    }
    /**complex coupling test*/
    @Test
    public void textCoup()
    {

        ComplexM zExpected = new ComplexM(2,-3);
        ComplexM zResult =new ComplexM();
        zResult=zResult.coupling(z1);
        testNumb=5;

        Assert.assertTrue(zExpected.equals(zResult));
    }
    /**complex exponenting test*/
    @Test
    public void testExponent()
    {

        double j=1;
        ComplexM zExpected = new ComplexM(2,3);
        ComplexM zResult =new ComplexM();
        zResult=zResult.exponent(z1,j);
        testNumb=6;

        Assert.assertTrue(zExpected.equals(zResult));

    }
    /**complex dividing by 0 test*/
    @Test(expected = IllegalArgumentException.class)
    public void testZeroDividing() {
        ComplexM zResult =new ComplexM();
        zResult = zResult.divide(z1,new ComplexM(0,0));
        testNumb=7;
    }
    /**test checking whether the value of the real or imaginary part of the complex exceed final values*/
    @Test
    public void testGreatestValue()
    {
        boolean toobig=false;
        ComplexM z1 =new ComplexM(132323.0,-2002002.0);
        if(z1.getImag() > MAX_VALUE || z1.getImag() < -MAX_VALUE || z1.getReal() > MAX_VALUE || z1.getReal() < -MAX_VALUE)
            toobig=true;
        testNumb=8;
        Assert.assertFalse(toobig);
    }
    /**test checking if the complex number representation would fill in the chart*/
    @Test
    public void testSizeInFrame()
    {
        boolean isDisproportion=false;
        ComplexM z1 =new ComplexM(11,45);
        if(z1.getReal()<XLEFT || z1.getReal()>XRIGHT || z1.getImag()<YDOWN || z1.getImag()>YUP)
            isDisproportion=true;
        testNumb=9;
        Assert.assertFalse(isDisproportion);
    }
    /**test checking if the typed string is a complex number*/
    @Test
    public void testTypedNumber()
    {
        boolean checkSense=false;
        String s="3+6i";
        try {
            z1 = u.stringConvert(s);
        }
        catch(NumberFormatException e)
        {
            checkSense=true;
        }
        testNumb=10;
        Assert.assertFalse(checkSense);

    }
}