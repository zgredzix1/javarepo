package model;
import org.apache.commons.math3.complex.Complex;

import static java.lang.Math.*;

/**
 *class Complex Represents complex number
 * private fields real and imag representing real and imaginary part of a complex number
 * public methods add (adding two complex numbers), diff( subtraction of two complex numbers)
 * mutliply( multiplying two complex numbers),exponent(raising a complex number to given power )
 * divide (dividing two complex numbers, while the other one isnt 0), coupling( complex number coupling)
 * toString ( printing a complex number)
 * @author Igor Rudnicki
 */
public class ComplexM {
    /**
     * variable real represents a real part of complex number
     */
    private double real;
    /**
     * variable imag represents an imaginary part of complex number
     */
    private double imag;

    public ComplexM()
    {
        real=0;
        imag=0;
    }
    public ComplexM(double real, double imag) {
        this.real = real;
        this.imag = imag;
    }

    /**
     * adding real parts of two complex numbers and assigning it to variable r
     * adding imaginary parts of two complex numbers and assigning it to variable r
     * @param z1 First complex number
     * @param z2 Second complex number
     * @return new object of class model.Complex
     */
    public ComplexM add(ComplexM z1,ComplexM z2) {
        double r=z1.real + z2.real;
        double i=z1.imag + z2.imag;
        return new ComplexM(r, i);
    }
    /**
     * substracting real parts of two complex numbers and assigning it to variable r
     * substracting imaginary parts of two complex numbers and assigning it to variable r
     * @param z1 First complex number
     * @param z2 Second complex number
     * @return new object of class model.Complex
     */
    public ComplexM diff(ComplexM z1,ComplexM z2) {
        double r=z1.real - z2.real;
        double i=z1.imag - z2.imag;
        return new ComplexM(r, i);
    }
    /**
     *multiplying two complex numbers using special formula
     * @param z1 First complex number
     * @param z2 Second complex number
     * @return new object of class model.Complex
     */
    public ComplexM multiply(ComplexM z1,ComplexM z2) {
        double r=z1.real * z2.real - z1.imag * z2.imag;
        double i=z1.real * z2.imag + z1.imag * z2.real;
        return new ComplexM(r,i);
    }

    /**
     * exponenting a complex number to given power
     * assigning z1 to new object of class model.Complex z2
     * @param z1 model.Complex number
     * @param j variable of int type representing power
     * @return new object of class model.Complex
     */
    public ComplexM exponent(ComplexM z1,double j)
    {
        double r=0;
        double im=0;
        ComplexM z2=z1;
        if(z1.imag==0)
        {
            r=pow(z1.real,j);
            im=0;
            return new ComplexM(r,im);
        }
        else
        {
            if(!Utils.isDoubleInt(j) || j<0) {
                Complex z = new Complex(z1.real, z1.imag);
                z = z.pow(j);
                return new ComplexM(z.getReal(), z.getImaginary());
            }
            else
                {
                    if(j==0)
                    {
                        return new ComplexM(1,0);
                    }
                    else if(j==1)
                    {
                        return z1;
                    }
                    else if(j>1)
                    {
                            for(int i=0;i<j-1;i++)
                            {
                                z1=z1.multiply(z1,z2);
                                r=z1.getReal();
                                im=z1.getImag();
                            }
                        return new ComplexM(r,im);
                    }
                    else
                        return new ComplexM(0,0);


                }

        }

    }
    /**
     * dividing two complex numbers using special formula
     * if imaginary part of the second number is 0, we divide by a constant number
     * @param z1 First complex number
     * @param z2 Second complex number
     * @return new object of class model.Complex
     */
    public ComplexM divide(ComplexM z1,ComplexM z2)
    {
        if(z2.imag==0 && z2.real==0)
            throw new IllegalArgumentException("Cannot divide by 0!");
        double r,i;
        if(z2.imag==0)
        {
            r=(z1.real/z2.real);
            i=(z1.imag/z2.real);
        }
        else {
            r = ((z1.real * z2.real + z1.imag * z2.imag)) / (z2.imag * z2.imag + z2.real * z2.real);
            i = ((z1.imag * z2.real - z1.real * z2.imag)) / (z2.imag * z2.imag + z2.real * z2.real);
        }

        return new ComplexM(r,i);
    }

    /**
     * changing a complex number into its couple
     * @param z1 complex number
     * @return new object of class model.Complex
     */
    public ComplexM coupling(ComplexM z1){
        if(z1.real==0)
            return new ComplexM(0,-1*z1.imag);
        else
            return new ComplexM(z1.real,-1*z1.imag);
    }
    public double getReal() {
        return real;
    }

    public double getImag() {
        return imag;
    }

    public void setReal(double real) {
        this.real = real;
    }

    public void setImag(double imag) {
        this.imag = imag;
    }

    /**
     *rounding to three decimal numbers the real and imaginary part
     * @return String which represents a complex number
     */
    @Override
    public String toString(){

        if(real==0 && imag==0)
            return 0+"+"+0+"i";
        else if (real==0)
            return imag+"i";
        else if (imag==0)
            return real+"";
        else if(imag<0)
            return real+""+imag+"i";
        else
            return real+"+"+imag+"i";
    }

    /**
     * method equals - checks if two objects are equal
     * @param o given objects, for us of class Complex
     * @return true if two objects are equal, false if not
     */
    @Override
    public boolean equals(Object o) {
        if(o==this)
            return true;
        if(!(o instanceof ComplexM))
            return false;

        ComplexM c= (ComplexM)o;
        return Double.compare(real,c.real) ==0
                && Double.compare(imag,c.imag)==0;

    }
}