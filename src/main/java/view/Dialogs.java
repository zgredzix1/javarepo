package view;
import controller.DialogController;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.KeyListener;

/**class Dialogs represents a DialogMessage which can be seen while error occurres
 * @see JDialog
 */
public class Dialogs extends JDialog  {

    /**size of the users screen*/
    private Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
    /**width of the user screen*/
    private int width = (int)screenSize.getWidth()/2-150;
    /**height of the user screen*/
    private int height = (int)screenSize.getHeight()/2-150;
    /**object of class DialogController*/
    private DialogController dc;
    /**JButton bExit used to exit the dialogmessage*/
    private JButton bExit;

    /**constructor of class Dialogs
     * setting its inner paramteters and creating a new object of class DialogController
     * @param word given message on each error
     */
    public Dialogs(String word)  {

        setTitle("ErrorMassage");
        setResizable(true);
        setModal(true);
        setLocation(width,height);
        setPreferredSize(new Dimension(550,100));

        bExit= new JButton(word);
        bExit.setPreferredSize(new Dimension(550,80));
        bExit.setFont(new Font("SansSerif",Font.BOLD,18));
        bExit.setForeground(Color.WHITE);
        bExit.setBackground(new Color(220,200,10));
        add(bExit);

        dc= new DialogController(this);
        setAlwaysOnTop(true);
        toFront();
        pack();
        setVisible(true);
    }

    /**method adding Keylistener and Actionlistener to bExit button
     * @param kal given ActionListener
     * @param mal given KeyListener
     */
    public void addExitandKeyListener(ActionListener kal,KeyListener mal)
    {
        bExit.addKeyListener(mal);
        bExit.addActionListener(kal);
    }
    /**method used to dispose the jdialog window*/
    public void dispose() {
        // do your work here
        super.dispose();
    }


}