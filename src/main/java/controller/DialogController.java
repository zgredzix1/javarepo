package controller;
import view.Dialogs;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

/**class DialogController implements KeyListener
 *
 * @see KeyListener
 * Used to handle and event connected with the exit button of JDialog
 * @author Igor Rudnicki
 */
public class DialogController {

    /**Object of class Dialogs*/
    private Dialogs d;

    /**Constructor of class DialogController
     * adding KeyListener to an exit button
     * @param d Object of class Dialogs
     */
    public DialogController(Dialogs d)
    {
        this.d=d;
        d.addExitandKeyListener(new ExitListener(),new KeysListener());
    }
    private class ExitListener implements ActionListener
    {
        public void actionPerformed(ActionEvent e) {
            System.out.println("s");
            d.dispose();
        }
    }
    private class KeysListener implements KeyListener
    {

        @Override
        public void keyTyped(java.awt.event.KeyEvent evt) {}
        @Override
        public void keyReleased(java.awt.event.KeyEvent evt) {}

        /**overrided method keyPressed
         * if clicked button is ENTER then we dispose the JDialog
         * @param evt KeyEvent
         */
        @Override
        public void keyPressed(java.awt.event.KeyEvent evt) {
            int key=evt.getKeyCode();
            if(key==KeyEvent.VK_ENTER)
                d.dispose();
        }
    }
}
